import 'package:flutter/material.dart';
import 'package:flutter_app_01/screence/note_details.dart';

class NoteList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return NoteListState();
  }
}

class NoteListState extends State<NoteList> {
  int count = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar( title: Text('Notes')),
      body: getNoteListView(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          debugPrint('FAB clicked');
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return NoteDetail();
          }));
          //navigateToDetails();
        },
        tooltip: 'Add Note',
        child: Icon(Icons.add),
      ),
    );
  }
}

ListView getNoteListView() {
  //TextStyle textStyle = Theme.of(context,shadowThemeOnly: false ).textTheme.subhead;

  var count = 0;
  return ListView.builder(
    itemCount: count,
    itemBuilder: (BuildContext context, int position) {
      return Card(
        color: Colors.white,
        elevation: 2.0,
        child: ListTile(
          leading: CircleAvatar(
            backgroundColor: Colors.yellow,
            child: Icon(Icons.keyboard_arrow_right),
          ),
          title: Text('Dumy Title'),
          subtitle: Text('Dumy Date'),
          trailing: Icon(
            Icons.delete,
            color: Colors.grey,
          ),
          onTap: () {
            debugPrint('ListTitle Tapped');
            //navigateToDetails();
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return NoteDetail();
            }));


          },
        ),
      );
    },
  );
}

/*void navigateToDetails() {
  Navigator.push(context, MaterialPageRoute( builder: (context) {
    return NoteDetail( );
  } ) );
}*/
